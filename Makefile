CXXFLAGS=-Wall \
		-Werror \
		-std=c++14 \
		`pkg-config --cflags sdl2` \
		-g -ggdb \
		-fPIC \
		-Isrc/ \

LDFLAGS= \
		`pkg-config --libs sdl2` \
		-g -ggdb \
		-fPIC \

SRCS= \
	  src/exception.cpp \
	  src/main.cpp \
	  src/story.cpp \
	  src/main_story.cpp \
	  src/system.cpp \
	  src/window.cpp \

TARGET=ariana

OBJS=$(SRCS:.cpp=.o)

.PHONY: build builder clean run

$(TARGET): $(OBJS)
	$(CXX) -o $@ $(OBJS) $(LDFLAGS)

builder:
	DOCKER_BUILDKIT=1 docker build --build-arg ARIANA_DEBIAN_VERSION=bullseye -t ariana .

build: builder
	docker run --rm \
		-u $(shell id -u) \
		-v $(PWD):/target:rw \
		ariana \
			make -j$(shell lscpu | awk '/^CPU\(s\):/ { print $$2 }')

run: build
	docker run --rm \
		-u $(shell id -u) \
		-v $(PWD):/target:rw \
		-v /tmp/.X11-unix:/tmp/.X11-unix \
		--device=/dev/dri:/dev/dri \
		--env="DISPLAY=$(DISPLAY)" \
		ariana

clean:
	-$(RM) $(TARGET) $(wildcard **/*.o)
