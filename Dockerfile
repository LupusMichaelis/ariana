# syntax=docker/dockerfile-upstream:master-labs
ARG ARIANA_DEBIAN_VERSION

FROM debian:$ARIANA_DEBIAN_VERSION

SHELL [ "/bin/bash", "-euo", "pipefail", "-c" ]
WORKDIR /target
ENV HOME=/target
CMD [ "./ariana" ]

RUN <<eos
declare -ar lib_dependencies=(
	xauth

	libsdl2-2.0-0
	libsdl2-ttf-2.0-0
)

declare -ar dev_dependencies=(
	gcc
	g++
	gdb
	make
	cgreen1
	lcov

	libboost-dev
	libcgreen1-dev
	libsdl2-dev
	libsdl2-ttf-dev
)

declare -ar packages=(
	${dev_dependencies[@]}
	${lib_dependencies[@]}
)

apt-get update
apt-get install -yqq ${packages[@]}
eos
