CXXFLAGS=-Wall \
		-Werror \
		-std=c++14 \
		`pkg-config --cflags sdl2` \
		-g -ggdb \
		-Isrc/ \
		-fPIC \

LDFLAGS= \
		-g -ggdb \
		-fPIC \

################################################################################
CXXFLAGS += \
		-I$(HOME)/include/ \
		--coverage \

LDFLAGS += \
		-L$(PWD)/include/ \
		-lcgreen \
		--coverage \

TESTDIR=./tests/

TESTS= \
	$(TESTDIR)/test_exception \
	$(TESTDIR)/test_window \
	$(TESTDIR)/test_story \
	$(TESTDIR)/test_main_story \
	$(TESTDIR)/test_system \

UNITTESTER=cgreen-runner --colours

.PHONY: all prep clean

all: prep $(TESTS)
	lcov --directory . --capture --output-file coverage.info
	lcov -r coverage.info "/usr*" -o coverage.info
	genhtml -o coverage coverage.info

prep:
	find src -name \*.h -exec basename '{}' \; | \
		while read header; \
		do ln -sf "../../src/$header" include/ariana/; \
		done

clean:
	$(RM) $(TESTS) $(wildcard **/*.o **/*.so **/*.gcda **/*.gcno)

tests/%.so: tests/%.o
	$(CXX) -shared -Wl,-soname,$@ -o $@ $^ $(LDFLAGS) -fPIC

tests/%.o: tests/%.cpp
	$(CXX) -o $@ -c $< $(CXXFLAGS)

# Exception ############################################################
$(TESTDIR)/test_exception.so: src/exception.o $(TESTDIR)/sdl_mocks.o $(TESTDIR)/test_exception.o
$(TESTDIR)/test_exception: $(TESTDIR)/test_exception.so
	$(UNITTESTER) $^

$(TESTDIR)/test_system.so: src/system.o src/story.o src/window.o src/exception.o $(TESTDIR)/sdl_mocks.o $(TESTDIR)/test_system.o
$(TESTDIR)/test_system: $(TESTDIR)/test_system.so
	$(UNITTESTER) $^

$(TESTDIR)/test_window.so: src/window.o src/exception.o $(TESTDIR)/sdl_mocks.o $(TESTDIR)/test_window.o
$(TESTDIR)/test_window: $(TESTDIR)/test_window.so
	$(UNITTESTER) $^

$(TESTDIR)/test_story.so: src/story.o src/exception.o $(TESTDIR)/sdl_mocks.o $(TESTDIR)/test_story.o
$(TESTDIR)/test_story: $(TESTDIR)/test_story.so
	$(UNITTESTER) $^

$(TESTDIR)/test_main_story.so: src/main_story.o src/story.o src/window.o src/exception.o $(TESTDIR)/sdl_mocks.o $(TESTDIR)/test_main_story.o
$(TESTDIR)/test_main_story: $(TESTDIR)/test_main_story.so
	$(UNITTESTER) $^
