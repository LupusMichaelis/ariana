#include <ariana/system.h>
#include <ariana/story.h>
#include <ariana/exception.h>

#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>

#include <sstream>

#include "sdl_mocks.h"

using namespace cgreen;

namespace ariana
{
	int const c_default_screen_width = 300;
	int const c_default_screen_height = 200;
}

Describe(ArianaSystem);
BeforeEach(ArianaSystem) {}
AfterEach(ArianaSystem) {}

Ensure(ArianaSystem, just_run_the_system)
{
	expect
		( SDL_Init
		, when(flags, is_equal_to(SDL_INIT_VIDEO))
		, will_return(0)
		);

	never_expect
		( SDL_GetError
		);

	expect
		( SDL_Quit
		);

	ariana::system s;
	s.run();
}

Ensure(ArianaSystem, fail_system_on_video_init)
{
	expect
		( SDL_Init
		, when(flags, is_equal_to(SDL_INIT_VIDEO))
		, will_return(-1)
		);

	expect
		( SDL_Quit
		);

	expect
		( SDL_GetError
		, will_return("No no no")
		);

	std::stringstream out;

	ariana::system s;
	s.set_logger(&out);
	s.run();

	char content[1024];
	out.getline(content, sizeof content);
	assert_that
		( content
		, is_equal_to_string("Could not initialize! SDL Error: No no no")
		);
}

Ensure(ArianaSystem, set_story)
{
	expect
		( SDL_Init
		, when(flags, is_equal_to(SDL_INIT_VIDEO))
		, will_return(0)
		);

	expect
		( SDL_Quit
		);

	struct local_story : public ariana::story
	{
		~local_story() { }
		bool start() { return true; }
		bool stop() { return false; }
		bool is_running() { return true; }
		ariana::window * get_display() { return nullptr; }
	};

	ariana::system s{};
	s.set_story<local_story>();
	s.run();
}
