#include <cstdint>
#include <cstddef>

#include <cgreen/mocks.h>

#include "sdl_mocks.h"

extern "C" {

int SDL_Init(Uint32 flags)
{
	using namespace cgreen;
	return mock(flags);
}

void SDL_Quit(void)
{
	using namespace cgreen;
	mock();
}

void SDL_Delay(Uint32 ms)
{
	using namespace cgreen;
	mock(ms);
}

void SDL_DestroyWindow(SDL_Window * window)
{
	using namespace cgreen;
	mock(window);
}

SDL_Window * SDL_CreateWindow(const char *title,
                              int x, int y, int w,
                              int h, Uint32 flags)
{
	using namespace cgreen;
	return (SDL_Window *) mock(title, x, y, w, h, flags);
}

int SDL_FillRect(SDL_Surface * dst, const SDL_Rect * rect, Uint32 color)
{
	using namespace cgreen;
	return (int) mock(dst, rect, color);
}

Uint32 SDL_MapRGB(const SDL_PixelFormat * format,
                  Uint8 r, Uint8 g, Uint8 b)
{
	using namespace cgreen;
	return (Uint32) mock(format, r, g, b);
}

const char* SDL_GetError(void)
{
	using namespace cgreen;
	return (char const *) mock();
}

int SDL_UpdateWindowSurfaceRects(SDL_Window * window,
                                 const SDL_Rect * rects,
                                 int numrects)
{
	using namespace cgreen;
	return (int) mock(window, rects, numrects);
}

int SDL_UpdateWindowSurface(SDL_Window * window)
{
	using namespace cgreen;
	return (int) mock(window);
}

SDL_Surface * SDL_GetWindowSurface(SDL_Window * window)
{
	using namespace cgreen;
	return (SDL_Surface *) mock(window);
}

}// extern "C"
