#include <ariana/exception.h>
#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>

#include "sdl_mocks.h"

using namespace cgreen;

Describe(ArianaException);
BeforeEach(ArianaException) {}
AfterEach(ArianaException) {}

Ensure(ArianaException, make_empty_message)
{
	auto e = ariana::make_exception();
	assert_that(e.what(), is_equal_to_string(""));
}

Ensure(ArianaException, make_message)
{
	auto e = ariana::make_exception("Some text");
	assert_that(e.what(), is_equal_to_string("Some text"));
}

Ensure(ArianaException, make_sdl_message)
{
	expect
		( SDL_GetError
		, will_return("yolo")
		);
	auto e = ariana::make_sdl_exception();
	assert_that(e.what(), is_equal_to_string("SDL Error: yolo"));
}

Ensure(ArianaException, make_sdl_extra_message)
{
	expect
		( SDL_GetError
		, will_return("yolo")
		);
	auto e = ariana::make_sdl_exception("Some");
	assert_that(e.what(), is_equal_to_string("Some SDL Error: yolo"));
}
