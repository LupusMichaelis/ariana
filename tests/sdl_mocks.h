#pragma once

extern "C" {

#define	SDL_INIT_TIMER		0x00000001
#define SDL_INIT_AUDIO		0x00000010
#define SDL_INIT_VIDEO		0x00000020
#define SDL_INIT_CDROM		0x00000100
#define SDL_INIT_JOYSTICK	0x00000200
#define SDL_INIT_NOPARACHUTE	0x00100000
#define SDL_INIT_EVENTTHREAD	0x01000000
#define SDL_INIT_EVERYTHING	0x0000FFFF

struct SDL_Window;
struct SDL_Surface;
struct SDL_Rect;
struct SDL_PixelFormat;

typedef uint32_t Uint32;
typedef uint8_t Uint8;

int SDL_Init(Uint32 flags);
void SDL_Quit(void);
void SDL_Delay(Uint32 ms);
void SDL_DestroyWindow(SDL_Window * window);
SDL_Window * SDL_CreateWindow(const char *title,int x, int y, int w,int h, Uint32 flags);
int SDL_FillRect(SDL_Surface * dst, const SDL_Rect * rect, Uint32 color);
Uint32 SDL_MapRGB(const SDL_PixelFormat * format, Uint8 r, Uint8 g, Uint8 b);
const char* SDL_GetError(void);
int SDL_UpdateWindowSurfaceRects(SDL_Window * window, const SDL_Rect * rects, int numrects);
int SDL_UpdateWindowSurface(SDL_Window * window);
SDL_Surface * SDL_GetWindowSurface(SDL_Window * window);

}// extern "C"
