#include <ariana/window.h>

#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>

#include "sdl_mocks.h"

using namespace cgreen;

Describe(ArianaWindow);
BeforeEach(ArianaWindow) {}
AfterEach(ArianaWindow) {}

Ensure(ArianaWindow, make_empty_message)
{
	struct {
		void * format;
	} window;
	expect
		( SDL_CreateWindow
		, will_return( (SDL_Window *) &window)
		);

	struct {
	} surface;
	expect
		( SDL_GetWindowSurface
		, will_return( (SDL_Surface *) &surface)
		);

	expect
		( SDL_MapRGB
		, will_return(0xffffff)
		);

	expect
		( SDL_FillRect
		, will_return(0)
		);

	expect
		( SDL_UpdateWindowSurface
		, will_return(0)
		);

	expect
		( SDL_DestroyWindow
		);

	ariana::window w;
	w.draw();
}
