#include <ariana/story.h>
#include <ariana/exception.h>

#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>

#include "sdl_mocks.h"

namespace ariana
{
struct window {};
}

class my_story
	: public ariana::story
{
	public:
		my_story() : main_window{}, running{false} {};
		bool start() { return running = !running; };
		bool stop() { return !(running = !running); };
		bool is_running() { return running; };
	private:
		ariana::window * get_display()
		{
			return &main_window;
		};

		ariana::window main_window;
		bool running;
} /* my_story */;

using namespace cgreen;

Describe(ArianaStory);
BeforeEach(ArianaStory) {}
AfterEach(ArianaStory) {}

Ensure(ArianaStory, start_a_story)
{
	std::shared_ptr<ariana::story> p_my_story = std::make_shared<my_story>();

	bool success = p_my_story->start();
	assert_true(success);
}

Ensure(ArianaStory, display_a_story_before_start)
{
	std::shared_ptr<ariana::story> p_my_story = std::make_shared<my_story>();

	assert_throws
		( ariana::exception
		, [&p_my_story]()
			{
				ariana::window & w = p_my_story->display();
				(void) w;
			}()
		);
}

Ensure(ArianaStory, display_a_story_after_start)
{
	std::shared_ptr<ariana::story> p_my_story = std::make_shared<my_story>();

	bool success = p_my_story->start();
	assert_true(success);

	ariana::window & w = p_my_story->display();
	(void) w;
}

Ensure(ArianaStory, stop_a_story_then_fail_display)
{
	std::shared_ptr<ariana::story> p_my_story = std::make_shared<my_story>();

	{
		bool success = p_my_story->start();
		assert_true(success);
	}

	{
		bool success = p_my_story->stop();
		assert_true(success);
	}

	assert_throws
		( ariana::exception
		, [&p_my_story]()
			{
				ariana::window & w = p_my_story->display();
				(void) w;
			}()
		);
}
