#include <ariana/main_story.h>
#include <ariana/exception.h>

#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>

#include "sdl_mocks.h"

namespace ariana
{
int const c_default_screen_width = 640;
int const c_default_screen_height = 480;
}

using namespace cgreen;

Describe(ArianaMainStory);
BeforeEach(ArianaMainStory) {}
AfterEach(ArianaMainStory) {}

Ensure(ArianaMainStory, just_run_the_system)
{
	/*
	expect
		( SDL_Init
		, when(flags, is_equal_to(SDL_INIT_VIDEO))
		, will_return(0)
		);
	*/

	never_expect
		( SDL_GetError
		);

	struct {
	} surface;
	expect
		( SDL_GetWindowSurface
		, will_return( (SDL_Surface *) &surface)
		);

	struct {
		void * format;
	} window;
	expect
		( SDL_CreateWindow
		, will_return( (SDL_Window *) &window)
		);

	expect
		( SDL_MapRGB
		, will_return(0xffffff)
		);

	expect
		( SDL_FillRect
		, will_return(0)
		);

	expect
		( SDL_UpdateWindowSurface
		, will_return(0)
		);

	expect
		( SDL_DestroyWindow
		);

	expect
		( SDL_Delay
		);

	ariana::main_story s{};
	bool started = s.start();

	assert_true(started);
}
