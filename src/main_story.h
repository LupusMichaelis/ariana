#pragma once

#include "forward.h"
#include "story.h"
#include "window.h"

#include <memory>

namespace ariana
{

class main_story
	: public ariana::story
{
	public:
		main_story();
		virtual ~main_story();
		virtual bool start();
		virtual bool stop();
		virtual bool is_running();

	protected:
		virtual window * get_display();

	private:
		void init();

	private:
		std::unique_ptr<window> mp_main_window;
} /* class main_story */;

}

