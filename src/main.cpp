#include "forward.h"
#include "system.h"
#include "story.h"
#include "main_story.h"

namespace ariana
{

int const c_default_screen_width = 640;
int const c_default_screen_height = 480;


} // namespace ariana

int main()
{
	ariana::system s;
	s.set_story<ariana::main_story>();
	return s.run();
}
