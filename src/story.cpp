#include "exception.h"
#include "story.h"

namespace ariana
{

story::~story()
{
}

window & story::display()
{
	if(!is_running())
		throw ariana::make_exception("Story isn't started");

	window * p_w = get_display();
	if(nullptr == p_w)
		throw ariana::make_exception("Display is not set");

	return *p_w;
}

}
