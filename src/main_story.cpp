#include "main_story.h"
#include "window.h"

#include <memory>
#include <SDL.h>

namespace ariana
{

main_story::main_story()
{
}

main_story::~main_story()
{
}

bool main_story::start()
{
	if(is_running())
		return false;

	init();
	return is_running();
}

bool main_story::stop()
{
	if(!is_running())
		return false;

	mp_main_window.release();
	return is_running();
}

bool main_story::is_running()
{
	return bool(mp_main_window);
}

window * main_story::get_display()
{
	return mp_main_window.get();
}

void main_story::init()
{
	window_details details
		{ .title = std::string{"My first draft"}
		, .position_size = box
			{ .x = SDL_WINDOWPOS_UNDEFINED
			, .y = SDL_WINDOWPOS_UNDEFINED
			, .w = c_default_screen_width
			, .h = c_default_screen_height
			}
		, .background = background_details
			{ color: 0xabcdef
			}
		};
	mp_main_window = std::make_unique<window>(details);
	mp_main_window->draw();

	SDL_Delay(2000);
}
}
