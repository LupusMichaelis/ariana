#pragma once

#include "forward.h"
#include "story.h"

#include <iosfwd>
#include <memory>
#include <type_traits>

namespace ariana
{

class system
{
	public:
		system();
		~system();

		system(system const&) = delete;

		int run() noexcept;
		void set_logger(std::ostream * out) noexcept;

		template <typename StoryT>
		void set_story();

	private:
		void init() noexcept(false);

		std::unique_ptr<story> mp_main_story;
		std::ostream * mp_log;

} /* class system */;

template <typename StoryT>
void system::set_story()
{
	mp_main_story = std::make_unique<StoryT>();
}

}
