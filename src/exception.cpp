#include "exception.h"

#include <boost/format.hpp>
#include <SDL.h>

namespace ariana
{


exception const make_sdl_exception(char const * message /* = nullptr*/)
{
	char const * sdl_error = SDL_GetError();

	if(nullptr == message)
	{
		char const * fmt = "SDL Error: %s";
		return (boost::format(fmt) % sdl_error).str();
	}
	else
	{
		char const * fmt = "%s SDL Error: %s";
		return (boost::format(fmt) % message % sdl_error).str();
	}
}

exception const make_exception(char const * message /* = nullptr*/)
{
	return std::string(nullptr == message ? "" : message);
}

exception::exception(std::string const & message)
	: logic_error {message}
{
}

}
