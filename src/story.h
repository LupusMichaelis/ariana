#pragma once

#include "forward.h"

#include <memory>

namespace ariana
{

class story
{
	public:
		virtual ~story();
		virtual bool start() = 0;
		virtual bool stop() = 0;
		virtual bool is_running() = 0;

		virtual window & display();

	protected:
		virtual window * get_display() = 0;

} /* class story */;

}
