#pragma once

#include "forward.h"

#include <string>

class SDL_Window;
class SDL_Surface;

namespace ariana {

struct box
{
	int x, y, w, h;
} /* struct box */;

struct color_details
{
	color_details() : red{0}, green{0}, blue{0} {};
	color_details(uint32_t hex)
		: red((hex >> (2 * 8)) & 0xff)
		, green((hex >> 8) & 0xff)
		, blue(hex & 0xff)
	{};
	char red;
	char green;
	char blue;
} /* struct color_details */;

struct background_details
{
	color_details color;
} /* struct background_details */;

struct window_details
{
	std::string title;
	box position_size;
	background_details background;

} /* struct window_details */;

class window
{
	public:
		window();
		explicit window(window_details const & wd);
		~window();

		void draw() noexcept(false);

	private:
		void init() noexcept(false);

		window_details m_details;

		SDL_Window * mp_sdl_window;
		SDL_Surface * mp_screen_surface;
} /* class window */;

}
