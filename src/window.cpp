#include "exception.h"
#include "forward.h"
#include "window.h"

#include <SDL.h>

namespace ariana
{

window::window()
	: m_details {}
	, mp_sdl_window {}
	, mp_screen_surface {}
{
}

window::window(window_details const & wd)
	: window {}
{
	m_details = wd;
}

void window::init() noexcept(false)
{
	if(nullptr == mp_sdl_window)
		mp_sdl_window = SDL_CreateWindow
			( m_details.title.c_str()
			, m_details.position_size.x
			, m_details.position_size.x
			, m_details.position_size.w
			, m_details.position_size.h
			, SDL_WINDOW_SHOWN
			);

	if(nullptr == mp_sdl_window)
		throw make_sdl_exception("Window could not be created!");
}

void window::draw() noexcept(false)
{
	init();

	mp_screen_surface = SDL_GetWindowSurface(mp_sdl_window);
	if(!mp_screen_surface)
		throw make_sdl_exception("SDL could not create window!");

	auto map_rgb = SDL_MapRGB
		( mp_screen_surface->format
		, m_details.background.color.red
		, m_details.background.color.green
		, m_details.background.color.blue
		);
	SDL_FillRect(mp_screen_surface, NULL, map_rgb);
	SDL_UpdateWindowSurface(mp_sdl_window);
}

window::~window()
{
    SDL_DestroyWindow(mp_sdl_window);
}

}
