#include "exception.h"
#include "system.h"
#include "window.h"

#include <cstdlib>
#include <iostream>
#include <SDL.h>
#include <memory>

namespace ariana
{

system::system()
	: mp_log { &std::cerr }
{
}

system::~system()
{
    SDL_Quit();
}

void system::init() noexcept(false)
{
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
		throw make_sdl_exception("Could not initialize!");

    if(mp_main_story
        && mp_main_story->start())
        mp_main_story->display();
}

int system::run() noexcept
{
	try
	{
		init();
		return EXIT_SUCCESS;
	}
	catch(exception const & e)
	{
		*mp_log << e.what() << "\n";
		return EXIT_FAILURE;
	}
}

void system::set_logger(std::ostream * out) noexcept
{
	mp_log = out;
}

}
