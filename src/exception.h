#pragma once

#include "forward.h"

#include <stdexcept>

namespace ariana
{

exception const make_exception(char const * message = nullptr);
exception const make_sdl_exception(char const * message = nullptr);

class exception
	: public std::logic_error
{
	public:
		exception(std::string const & message);
} /* class exception */;

}
